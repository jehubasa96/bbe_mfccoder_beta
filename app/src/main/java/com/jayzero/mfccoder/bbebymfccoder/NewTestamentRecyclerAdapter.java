package com.jayzero.mfccoder.bbebymfccoder;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by jayzero on 25/07/2016.
 */

public class NewTestamentRecyclerAdapter extends RecyclerView.Adapter<NewTestamentRecyclerAdapter.NTViewHolder> {
    private LayoutInflater mLayoutInflate;
    NTInterface NTInterface;
    private Context context;

    List<NewTestamentContent> contents = Collections.emptyList();

    String[] books = {"MATTHEW", "MARK", "LUKE", "JOHN", "ACTS", "ROMANS", "1 CORINTHIANS"
            , "2 CORINTHIANS", "GALATIANS", "EPHESIANS", "PHILIPPIANS", "COLOSSIANS", "1 THESSALONIANS", "2 THESSALONIANS", "1 TIMOTHY"
            , "2 TIMOTHY", "TITUS", "PHILEMON", "HEBREWS", "JAMES", "1 PETER", "2 PETER", "1 JOHN"
            , "2 JOHN", "3 JOHN", "JUDE", "REVELATION"};

    public NewTestamentRecyclerAdapter(Context context, List<NewTestamentContent> contents) {
        mLayoutInflate = LayoutInflater.from(context);
        this.context = context;
        this.contents = contents;
    }

    public void setNTInterface(NTInterface NTInterface) {
        this.NTInterface = NTInterface;
    }

    @Override
    public NTViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mLayoutInflate.inflate(R.layout.newtestamen_recyclervh, parent, false);
        NTViewHolder o = new NTViewHolder(v);
        return o;
    }

    @Override
    public void onBindViewHolder(final NTViewHolder holder, int position) {
        NewTestamentContent n = contents.get(position);
        Log.d("onBind: ", Integer.toString(n.book) + " " + Integer.toString(n.chapter));
        holder.verse.setText(Integer.toString(n.verse));
        holder.text.setText(n.bibleContent);
        NTInterface.getBibleName(n.book - 39);
        NTInterface.getChapter(n.chapter);

        holder.v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                CharSequence[] options = {"Bookmark", "Send as SMS", "Copy to Clipboard"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("OPTION").setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                BookMarkDatabaseHelper bookmark = new BookMarkDatabaseHelper(context);
                                NewTestamentContent bookm = contents.get(holder.getAdapterPosition());

                                boolean result = bookmark.insertData(bookm._id, bookm.book, bookm.chapter, bookm.verse, bookm.bibleContent);
                                if (result) {
                                    Toast.makeText(context, "Saved as bookmark", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(context, "Saving as bookmark failed", Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 1:
                                NewTestamentContent nSendContents = contents.get(holder.getAdapterPosition());
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse("smsto:"));
                                intent.putExtra("sms_body", books[nSendContents.book - 40] + " " + nSendContents.chapter + ":" + nSendContents.verse + "\n" + nSendContents.bibleContent);
                                context.startActivity(intent);
                                break;
                            case 2:
                                NewTestamentContent nCopyContents = contents.get(holder.getAdapterPosition());
                                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                                ClipData clipData = ClipData.newPlainText("copy", books[nCopyContents.book - 40] + " " + nCopyContents.chapter + ":" + nCopyContents.verse + "\n" + nCopyContents.bibleContent);
                                clipboard.setPrimaryClip(clipData);

                                if (clipboard.hasPrimaryClip()) {
                                    Toast.makeText(context, "Copied to Clipboard", Toast.LENGTH_SHORT).show();
                                }
                                break;
                        }
                    }
                });

                AlertDialog a = builder.create();
                a.show();

                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    public class NTViewHolder extends RecyclerView.ViewHolder {
        TextView verse, text;
        public View v;

        public NTViewHolder(View itemView) {
            super(itemView);
            v = itemView;
            verse = (TextView) itemView.findViewById(R.id.ntVerseText);
            text = (TextView) itemView.findViewById(R.id.ntBibleText);
        }
    }

    public interface NTInterface {
        void getBibleName(int name);
        void getChapter(int num);
    }


}
