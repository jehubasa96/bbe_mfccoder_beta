package com.jayzero.mfccoder.bbebymfccoder;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class SearchActivity extends AppCompatActivity {
    private EditText searchField;
    private ImageButton mNTSButton, mOTSButton;
    private TextView noResult;
    private RecyclerView mSearchResultRecycler;
    private SearchRecyclerAdapter mSRecyclerAdapter;
    private OTDatabaseHelper mOTDH;
    private NTDatabaseHelper mNTDH;

    String[] booksOT = {"GENESIS", "EXODUS", "LEVITICUS", "NUMBERS", "DEUTERONOMY", "JOSHUA", "JUDGES"
            , "RUTH", "1 SAMUEL", "2 SAMUEL", "1 KINGS", "2 KINGS", "1 CHRONICLES", "2 CHRONICLES", "EZRA"
            , "NEHEMIAH", "ESTHER", "JOB", "PSALMS", "PROVERBS", "ECCLESIASTES", "SONG OF SONGS", "ISAIAH"
            , "JEREMIAH", "LAMENTATIONS", "EZEKIEL", "DANIEL", "HOSEA", "JOEL", "AMOS", "OBADIAH", "JONAH"
            , "MICAH", "NAHUM", "HABAKKUK", "ZEPHANIAH", "HAGGAI", "ZECHARIAH", "MALACHI"};

    String[] booksNT = {"MATTHEW", "MARK", "LUKE", "JOHN", "ACTS", "ROMANS", "1 CORINTHIANS"
            , "2 CORINTHIANS", "GALATIANS", "EPHESIANS", "PHILIPPIANS", "COLOSSIANS", "1 THESSALONIANS", "2 THESSALONIANS", "1 TIMOTHY"
            , "2 TIMOTHY", "TITUS", "PHILEMON", "HEBREWS", "JAMES", "1 PETER", "2 PETER", "1 JOHN"
            , "2 JOHN", "3 JOHN", "JUDE", "REVELATION"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        searchField = (EditText) findViewById(R.id.searchField);
        noResult = (TextView) findViewById(R.id.noResultText);
        mOTSButton = (ImageButton) findViewById(R.id.OTImageButton);
        mNTSButton = (ImageButton) findViewById(R.id.NTImageButton);

        mSearchResultRecycler = (RecyclerView) findViewById(R.id.searchRecycler);

    }


    public void onButtonClick(View v) {
        switch (v.getId()) {
            case R.id.OTImageButton:
                mOTSButton.setImageResource(R.drawable.ot_on);
                String searchOT = searchField.getText().toString();
                noResult.setVisibility(View.GONE);
                new SearchOT(this, searchOT).execute();
                mNTSButton.setImageResource(R.drawable.nt_off);
                Log.d("button", "OT");
                break;
            case R.id.NTImageButton:
                mNTSButton.setImageResource(R.drawable.nt_on);
                String searchNT = searchField.getText().toString();
                noResult.setVisibility(View.GONE);
                new SearchNT(this, searchNT).execute();
                Log.d("button", "NT");
                mOTSButton.setImageResource(R.drawable.ot_off);
                break;
        }
    }

    @Override
    public void onBackPressed() {
    }

    public void onHomeClick(View v) {
        Intent n = new Intent(getBaseContext(), MainActivity.class);
        startActivity(n);
        finish();
    }


    class SearchOT extends AsyncTask<Integer, String, Integer> {
        private ProgressDialog mProgress;

        String search;
        Context context;
        int resultCount;

        public SearchOT(Context context, String search) {
            this.search = search;
            this.context = context;
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            mOTDH = new OTDatabaseHelper(context);
            List<SearchResultContents> result = new ArrayList<>();

            mOTDH.openDatabase();
            mOTDH.cursor = mOTDH.mDatabase.rawQuery("SELECT * FROM " + mOTDH.TABLE_NAME, null);
            mOTDH.cursor.moveToFirst();
            while (!mOTDH.cursor.isAfterLast()) {
                String word = mOTDH.cursor.getString(4);
                String[] compare = word.split(" ");

                if (mOTDH.cursor.getInt(1) < 40) {
                    for (int x = 0; x < compare.length; x++) {
                        if (compare[x].equals(search)) {
                            SearchResultContents searchResultContents = new SearchResultContents
                                    (mOTDH.cursor.getInt(0), booksOT[mOTDH.cursor.getInt(1)], mOTDH.cursor.getInt(2), mOTDH.cursor.getInt(3), mOTDH.cursor.getString(4));
                            result.add(searchResultContents);
                            break;
                        }
                    }
                }

                mOTDH.cursor.moveToNext();
            }

            mSRecyclerAdapter = new SearchRecyclerAdapter(context, result);
            resultCount = result.size();
            mOTDH.cursor.close();
            mOTDH.closeDatabase();

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress = ProgressDialog.show(context, "Searching", "Please Wait!!!");
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            mProgress.dismiss();
            if (resultCount == 0) {
                noResult.setVisibility(View.VISIBLE);
            }
            Toast.makeText(context, resultCount + " results", Toast.LENGTH_SHORT).show();
            mSearchResultRecycler.setAdapter(mSRecyclerAdapter);
            mSearchResultRecycler.setLayoutManager(new LinearLayoutManager(context));
        }


    }

    class SearchNT extends AsyncTask<Integer, String, Integer> {
        private ProgressDialog mProgress;

        String search;
        Context context;
        int resultCount;

        public SearchNT(Context context, String search) {
            this.search = search;
            this.context = context;
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            mNTDH = new NTDatabaseHelper(context);
            List<SearchResultContents> result = new ArrayList<>();

            mNTDH.openDatabase();
            mNTDH.cursor = mNTDH.mDatabase.rawQuery("SELECT * FROM " + mNTDH.TABLE_NAME, null);
            mNTDH.cursor.moveToFirst();
            while (!mNTDH.cursor.isAfterLast()) {
                String word = mNTDH.cursor.getString(4);
                String[] compare = word.split(" ");

                if (mNTDH.cursor.getInt(1) >= 40) {
                    for (int x = 0; x < compare.length; x++) {
                        if (compare[x].equals(search)) {
                            SearchResultContents searchResultContents = new SearchResultContents
                                    (mNTDH.cursor.getInt(0), booksNT[mNTDH.cursor.getInt(1) - 40], mNTDH.cursor.getInt(2), mNTDH.cursor.getInt(3), mNTDH.cursor.getString(4));
                            result.add(searchResultContents);
                            break;
                        }
                    }
                }

                mNTDH.cursor.moveToNext();
            }

            mSRecyclerAdapter = new SearchRecyclerAdapter(context, result);
            resultCount = result.size();
            mNTDH.cursor.close();
            mNTDH.closeDatabase();

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress = ProgressDialog.show(context, "Searching", "Please Wait!!!");
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            mProgress.dismiss();
            if (resultCount == 0) {
                noResult.setVisibility(View.VISIBLE);
            }
            Toast.makeText(context, resultCount + " results", Toast.LENGTH_SHORT).show();
            mSearchResultRecycler.setAdapter(mSRecyclerAdapter);
            mSearchResultRecycler.setLayoutManager(new LinearLayoutManager(context));
        }


    }
}
