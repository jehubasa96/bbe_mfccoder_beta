package com.jayzero.mfccoder.bbebymfccoder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by jayzero on 24/07/2016.
 */

public class NavigationDrawerListAdapter extends RecyclerView.Adapter<NavigationDrawerListAdapter.NavigationDrawerListVHolder> {
    private LayoutInflater mCustomLayout;
    private ClickListner mClickListener;
    List<NavigationDrawerListName> mTitles = Collections.emptyList();

    public NavigationDrawerListAdapter(Context context, List<NavigationDrawerListName> list) {
        mCustomLayout = LayoutInflater.from(context);
        mTitles = list;
    }

    public void setClickListener(ClickListner mClickListener) {
        this.mClickListener = mClickListener;
    }

    @Override
    public NavigationDrawerListVHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vLayout = mCustomLayout.inflate(R.layout.navdrawerlist_custom_layout, parent, false);
        NavigationDrawerListVHolder n = new NavigationDrawerListVHolder(vLayout);
        return n;
    }

    @Override
    public void onBindViewHolder(NavigationDrawerListVHolder holder, int position) {
        NavigationDrawerListName n = mTitles.get(position);
        holder.image.setImageResource(n.icon);
        holder.text.setText(n.title);
    }

    @Override
    public int getItemCount() {
        return mTitles.size();
    }

    class NavigationDrawerListVHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView text;
        ImageView image;

        public NavigationDrawerListVHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            text = (TextView) itemView.findViewById(R.id.navListText);
            image = (ImageView) itemView.findViewById(R.id.navListImage);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) {
                mClickListener.navItemClicked(v, getAdapterPosition());
            }
        }
    }

    public interface ClickListner {
        void navItemClicked(View v, int positon);
    }
}
