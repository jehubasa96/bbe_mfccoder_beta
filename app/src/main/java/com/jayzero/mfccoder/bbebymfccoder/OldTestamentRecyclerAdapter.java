package com.jayzero.mfccoder.bbebymfccoder;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by jayzero on 25/07/2016.
 */

public class OldTestamentRecyclerAdapter extends RecyclerView.Adapter<OldTestamentRecyclerAdapter.OTViewHolder> {
    private LayoutInflater mLayoutInflate;
    OTInterface otInterface;
    Context context;

    List<OldTestamentContent> contents = Collections.emptyList();

    String[] books = {"GENESIS", "EXODUS", "LEVITICUS", "NUMBERS", "DEUTERONOMY", "JOSHUA", "JUDGES"
            , "RUTH", "1 SAMUEL", "2 SAMUEL", "1 KINGS", "2 KINGS", "1 CHRONICLES", "2 CHRONICLES", "EZRA"
            , "NEHEMIAH", "ESTHER", "JOB", "PSALMS", "PROVERBS", "ECCLESIASTES", "SONG OF SONGS", "ISAIAH"
            , "JEREMIAH", "LAMENTATIONS", "EZEKIEL", "DANIEL", "HOSEA", "JOEL", "AMOS", "OBADIAH", "JONAH"
            , "MICAH", "NAHUM", "HABAKKUK", "ZEPHANIAH", "HAGGAI", "ZECHARIAH", "MALACHI"};

    public OldTestamentRecyclerAdapter(Context context, List<OldTestamentContent> contents) {
        mLayoutInflate = LayoutInflater.from(context);
        this.contents = contents;
        this.context = context;
    }

    public void setOtInterface(OTInterface otInterface) {
        this.otInterface = otInterface;
    }

    @Override
    public OTViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mLayoutInflate.inflate(R.layout.oldtestamen_recyclervh, parent, false);
        OTViewHolder o = new OTViewHolder(v);
        return o;
    }

    @Override
    public void onBindViewHolder(final OTViewHolder holder, int position) {
        OldTestamentContent n = contents.get(position);
        holder.verse.setText(Integer.toString(n.verse));
        holder.text.setText(n.bibleContent);
        otInterface.getBibleName(n.book);
        otInterface.getChapter(n.chapter);

        holder.v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                CharSequence[] options = {"Bookmark", "Send as SMS", "Copy to Clipboard"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("OPTION").setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                BookMarkDatabaseHelper bookmark = new BookMarkDatabaseHelper(context);
                                OldTestamentContent bookm = contents.get(holder.getAdapterPosition());

                                boolean result = bookmark.insertData(bookm._id, bookm.book, bookm.chapter, bookm.verse, bookm.bibleContent);
                                if (result) {
                                    Toast.makeText(context, "Saved as bookmark", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(context, "Saving as bookmark failed", Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 1:
                                OldTestamentContent oSendContents = contents.get(holder.getAdapterPosition());
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse("smsto:"));
                                intent.putExtra("sms_body", books[oSendContents.book - 1] + " " + oSendContents.chapter + ":" + oSendContents.verse + "\n" + oSendContents.bibleContent);
                                context.startActivity(intent);
                                break;
                            case 2:
                                OldTestamentContent oCopyContents = contents.get(holder.getAdapterPosition());
                                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                                ClipData clipData = ClipData.newPlainText("copy", books[oCopyContents.book - 1] + " " + oCopyContents.chapter + ":" + oCopyContents.verse + "\n" + oCopyContents.bibleContent);
                                clipboard.setPrimaryClip(clipData);

                                if (clipboard.hasPrimaryClip()) {
                                    Toast.makeText(context, "Copied to Clipboard", Toast.LENGTH_SHORT).show();
                                }
                                break;
                        }
                    }
                });

                AlertDialog a = builder.create();
                a.show();
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    public class OTViewHolder extends RecyclerView.ViewHolder {
        TextView verse, text;
        View v;

        public OTViewHolder(View itemView) {
            super(itemView);
            v = itemView;
            verse = (TextView) itemView.findViewById(R.id.otVerseText);
            text = (TextView) itemView.findViewById(R.id.otBibleText);
        }
    }

    public interface OTInterface {
        void getBibleName(int name);
        void getChapter(int num);
    }
}
