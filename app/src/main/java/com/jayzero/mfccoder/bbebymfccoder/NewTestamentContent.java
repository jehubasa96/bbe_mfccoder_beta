package com.jayzero.mfccoder.bbebymfccoder;

/**
 * Created by jayzero on 25/07/2016.
 */

public class NewTestamentContent {
    int _id, book, chapter, verse;
    String bibleContent;

    public NewTestamentContent(int _id, int book, int chapter, int verse, String text) {
        this.verse = verse;
        bibleContent = text;
        this.book = book;
        this.chapter = chapter;
        this._id = _id;
    }

}
