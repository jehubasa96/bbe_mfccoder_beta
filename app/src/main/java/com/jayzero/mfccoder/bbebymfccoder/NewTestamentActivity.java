package com.jayzero.mfccoder.bbebymfccoder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

public class NewTestamentActivity extends AppCompatActivity {
    private RecyclerView mBibleContent;
    private NewTestamentRecyclerAdapter mAdapter;
    private NTDatabaseHelper mNTDatabase;
    private BookMarkDatabaseHelper mBookmarkHelper;

    private Spinner mSpinnerBook, mSpinnerChap;
    private ArrayAdapter<CharSequence> mAAdapterBook;
    private ArrayAdapter<String> mAAdapterChap;

    private TextView mBookid, mChapId;
    private ImageButton home;

    boolean isTouched = false;

    String[] books = {"MATTHEW", "MARK", "LUKE", "JOHN", "ACTS", "ROMANS", "1 CORINTHIANS"
            , "2 CORINTHIANS", "GALATIANS", "EPHESIANS", "PHILIPPIANS", "COLOSSIANS", "1 THESSALONIANS", "2 THESSALONIANS", "1 TIMOTHY"
            , "2 TIMOTHY", "TITUS", "PHILEMON", "HEBREWS", "JAMES", "1 PETER", "2 PETER", "1 JOHN"
            , "2 JOHN", "3 JOHN", "JUDE", "REVELATION"};

    String[] chapters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_testament);

        mSpinnerBook = (Spinner) findViewById(R.id.bookSpinner);
        mSpinnerChap = (Spinner) findViewById(R.id.chapterSpinner);
        mAAdapterBook = ArrayAdapter.createFromResource(this, R.array.ntbooks, R.layout.spinner_item_layout);
        mAAdapterBook.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerBook.setAdapter(mAAdapterBook);

        mBookid = (TextView) findViewById(R.id.bookName);
        mChapId = (TextView) findViewById(R.id.chapterId);
        home = (ImageButton) findViewById(R.id.homebutton);

        mNTDatabase = new NTDatabaseHelper(this);

        mBibleContent = (RecyclerView) findViewById(R.id.ntContentList);
        mAdapter = new NewTestamentRecyclerAdapter(this, mNTDatabase.getNT());
        mBibleContent.setAdapter(mAdapter);
        mBibleContent.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.setNTInterface(new NewTestamentRecyclerAdapter.NTInterface() {
            @Override
            public void getBibleName(int name) {
                mBookid.setText(books[name - 1]);
            }

            @Override
            public void getChapter(int num) {
                mChapId.setText("CHAPTER " + num);
            }
        });

    }

    public void onNavigationclick(View v) {

        ImageButton nav = (ImageButton) findViewById(R.id.navigationWheel);
        if (!isTouched) {
            nav.setImageResource(R.drawable.ship_wheel_back);
            home.setVisibility(View.INVISIBLE);
            mSpinnerBook.setVisibility(View.VISIBLE);
            mSpinnerChap.setVisibility(View.VISIBLE);
            mSpinnerBook.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String s = Integer.toString(position + 1 + 39);
                    Log.d("mSpinnerBook: ", s);
                    navigateBook(s);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            isTouched = true;
        } else {
            nav.setImageResource(R.drawable.ship_wheel_512);
            home.setVisibility(View.VISIBLE);
            mSpinnerBook.setVisibility(View.INVISIBLE);
            mSpinnerChap.setVisibility(View.INVISIBLE);
            isTouched = false;

        }
    }

    public void onHomeClick(View v) {
        Intent n = new Intent(getBaseContext(), MainActivity.class);
        startActivity(n);
        finish();
    }

    public void navigateBook(String pos) {
        String string = pos + "001001";
        String nextString = (Integer.parseInt(pos) + 1) + "001001";
        int numString = Integer.parseInt(string);
        int numNextString = Integer.parseInt(nextString);


        mNTDatabase.openDatabase();
        int bookCount = 2;
        int[] bookSave = new int[1];

        mNTDatabase.cursor = mNTDatabase.mDatabase.rawQuery("SELECT * FROM " + mNTDatabase.TABLE_NAME, null);
        mNTDatabase.cursor.moveToFirst();
        while (!mNTDatabase.cursor.isAfterLast()) {
            if (mNTDatabase.cursor.getInt(1) > 39) {
                bookCount += 1;
                if (numString == mNTDatabase.cursor.getInt(0)) {
                    mBibleContent.scrollToPosition(bookCount);
                    bookSave[0] = bookCount;
                    Log.d("1bookcount: ", String.valueOf(bookCount));
                }
                Log.d("1bookcount: ", String.valueOf(mNTDatabase.cursor.getInt(0)));
                if (numNextString == mNTDatabase.cursor.getInt(0)) {
                    mNTDatabase.cursor.moveToPrevious();
                    setChapter(pos, mNTDatabase.cursor.getInt(2));
                    mNTDatabase.cursor.moveToLast();
                }
            }
            mNTDatabase.cursor.moveToNext();
        }
        mNTDatabase.cursor.close();
        mNTDatabase.closeDatabase();
    }

    public void setChapter(final String chap, int numChap) {
        Log.d("nitems: ", String.valueOf(numChap));
        chapters = new String[numChap];

        for (int x = 0; x < chapters.length; x++) {
            chapters[x] = String.valueOf(x + 1);
        }

        mAAdapterChap = new ArrayAdapter<String>(this, R.layout.spinner_item_layout, chapters);
        mAAdapterChap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerChap.setAdapter(mAAdapterChap);

        mSpinnerChap.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String s = String.valueOf(position + 1);
                if (s.length() < 2) {
                    s = "00" + s;
                    Log.d("1string length:", String.valueOf(s.length()));
                    navigateChapter(chap, s);
                } else if (s.length() == 2) {
                    s = "0" + s;
                    navigateChapter(chap, s);
                    Log.d("2string length:", String.valueOf(s.length()));
                } else {
                    navigateChapter(chap, s);
                    Log.d("3string length:", String.valueOf(s.length()));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void navigateChapter(String position, String s) {
        String string = position + s + "001";
        Log.d("string ", string);
        int numString = Integer.parseInt(string);
        int count = 2;

        mNTDatabase.openDatabase();
        mNTDatabase.cursor = mNTDatabase.mDatabase.rawQuery("SELECT * FROM " + mNTDatabase.TABLE_NAME, null);
        mNTDatabase.cursor.moveToFirst();
        while (!mNTDatabase.cursor.isAfterLast()) {

            if (mNTDatabase.cursor.getInt(1) > 39) {
                count++;
                if (numString == mNTDatabase.cursor.getInt(0)) {
                    mBibleContent.scrollToPosition(count);
                    mNTDatabase.cursor.isAfterLast();
                }
            }

            mNTDatabase.cursor.moveToNext();
        }
        mNTDatabase.cursor.close();
        mNTDatabase.closeDatabase();
    }

    @Override
    public void onBackPressed() {
    }
}
