package com.jayzero.mfccoder.bbebymfccoder;

/**
 * Created by jayzero on 28/07/2016.
 */

public class SearchResultContents {
    int id, chapter, verse;
    String text, book;

    public SearchResultContents(int id, String book, int chapter, int verse, String text) {
        this.id = id;
        this.book = book;
        this.chapter = chapter;
        this.verse = verse;
        this.text = text;
    }
}
