package com.jayzero.mfccoder.bbebymfccoder;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jayzero on 25/07/2016.
 */

public class NTDatabaseHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "bible";
    public static final String TABLE_NAME = "t_bbe";
    public static final String DB_LOCATION = "/data/data/com.jayzero.mfccoder.bbebymfccoder/databases/";
    private Context mContext;
    public SQLiteDatabase mDatabase;
    public Cursor cursor;

    public NTDatabaseHelper(Context c) {
        super(c, DB_NAME, null, 3);
        this.mContext = c;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        CopyDataBase n = new CopyDataBase();
        if (newVersion > oldVersion) {
            n.execute();
        }
    }

    public void openDatabase() {
        String dbPath = mContext.getDatabasePath(DB_NAME).getPath();
        if (mDatabase != null && mDatabase.isOpen()) {
            return;
        } else {
            mDatabase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READONLY);
        }

    }

    public void closeDatabase() {
        if (mDatabase != null) {
            mDatabase.close();
        }
    }

    public List<NewTestamentContent> getNT() {
        List<NewTestamentContent> mNT = new ArrayList<>();
        openDatabase();
        cursor = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (cursor.getInt(1) > 39) {
                NewTestamentContent mNTContent = new NewTestamentContent(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getString(4));
                mNT.add(mNTContent);
            }
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();

        return mNT;

    }

}
