package com.jayzero.mfccoder.bbebymfccoder;

import android.util.Log;

/**
 * Created by jayzero on 25/07/2016.
 */

public class OldTestamentContent {
    int _id, book, chapter, verse;
    String bibleContent;

    public OldTestamentContent(int _id, int book, int chapter, int verse, String text) {
        this.verse = verse;
        bibleContent = text;
        this.book = book;
        this.chapter = chapter;
        this._id = _id;
    }

}
