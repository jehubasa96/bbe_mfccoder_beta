package com.jayzero.mfccoder.bbebymfccoder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jayzero on 29/07/2016.
 */

public class BookMarkDatabaseHelper extends SQLiteOpenHelper {
    public static final String DATA_BASE_NAME = "Bookmarks";
    public static final String DATA_TABLE_NAME = "t_bookmarks";
    public static final String Col1 = "_id";
    public static final String Col2 = "book";
    public static final String Col3 = "chapter";
    public static final String Col4 = "verse";
    public static final String Col5 = "text";

    String[] books = {"GENESIS", "EXODUS", "LEVITICUS", "NUMBERS", "DEUTERONOMY", "JOSHUA", "JUDGES"
            , "RUTH", "1 SAMUEL", "2 SAMUEL", "1 KINGS", "2 KINGS", "1 CHRONICLES", "2 CHRONICLES", "EZRA"
            , "NEHEMIAH", "ESTHER", "JOB", "PSALMS", "PROVERBS", "ECCLESIASTES", "SONG OF SONGS", "ISAIAH"
            , "JEREMIAH", "LAMENTATIONS", "EZEKIEL", "DANIEL", "HOSEA", "JOEL", "AMOS", "OBADIAH", "JONAH"
            , "MICAH", "NAHUM", "HABAKKUK", "ZEPHANIAH", "HAGGAI", "ZECHARIAH", "MALACHI", "MATTHEW", "MARK"
            , "LUKE", "JOHN", "ACTS", "ROMANS", "1 CORINTHIANS", "2 CORINTHIANS", "GALATIANS", "EPHESIANS"
            , "PHILIPPIANS", "COLOSSIANS", "1 THESSALONIANS", "2 THESSALONIANS", "1 TIMOTHY", "2 TIMOTHY"
            , "TITUS", "PHILEMON", "HEBREWS", "JAMES", "1 PETER", "2 PETER", "1 JOHN", "2 JOHN", "3 JOHN"
            , "JUDE", "REVELATION"};


    public BookMarkDatabaseHelper(Context context) {
        super(context, DATA_BASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DATA_TABLE_NAME + " (" + Col1 + " INTEGER PRIMARY KEY, " + Col2 + " INTEGER, " + Col3 + " INTEGER, "
                + Col4 + " INTEGER, " + Col5 + " TEXT" + ")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATA_TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(int id, int book, int chapter, int verse, String text) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Col1, id);
        contentValues.put(Col2, book);
        contentValues.put(Col3, chapter);
        contentValues.put(Col4, verse);
        contentValues.put(Col5, text);
        long result = db.insert(DATA_TABLE_NAME, null, contentValues);
        if (result == -1) {
            return false;
        } else return true;
    }

    public Integer deleteData(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(DATA_TABLE_NAME, " _id = ? ", new String[]{id});
    }

    public List<BookmarkContents> getBookmarks() {
        Cursor c;
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("cursor: ", "1");
        List<BookmarkContents> list = new ArrayList<>();
        Log.d("cursor: ", "2");
        c = db.rawQuery("SELECT * FROM " + DATA_TABLE_NAME, null);
        Log.d("cursor: ", "3");
        c.moveToFirst();
        Log.d("cursor: ", "4");
        while (!c.isAfterLast()) {

            BookmarkContents cont = new BookmarkContents(c.getInt(0), books[c.getInt(1) - 1], c.getInt(2), c.getInt(3), c.getString(4));
            list.add(cont);
            Log.d("cursor: ", String.valueOf(c.getInt(1)));

            c.moveToNext();
        }
        c.close();
        return list;
    }
}
