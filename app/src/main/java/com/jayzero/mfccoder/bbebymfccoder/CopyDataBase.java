package com.jayzero.mfccoder.bbebymfccoder;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by jayzero on 26/07/2016.
 */

public class CopyDataBase extends AsyncTask<Integer, String, Integer> {
    private Context myContext;
    private ProgressDialog mPDialog;
    boolean isSucess;

    public CopyDataBase() {
    }

    public CopyDataBase(Context context) {
        myContext = context;
    }

    @Override
    protected Integer doInBackground(Integer... params) {
        OTDatabaseHelper myOTDBHelper = new OTDatabaseHelper(myContext);
        File directory = new File("/data/data/com.jayzero.mfccoder.bbebymfccoder/databases/");
        directory.mkdir();
        if (!myOTDBHelper.checkDataBase()) {
            try {
                InputStream inputStream = myContext.getAssets().open(myOTDBHelper.DB_NAME);
                Log.d("Copy 1: ", "done");
                String outFilename = myOTDBHelper.DB_LOCATION + myOTDBHelper.DB_NAME;
                Log.d("Copy 2: ", "done");
                OutputStream outputStream = new FileOutputStream(outFilename);
                Log.d("Copy 3: ", "done");
                byte[] buffer = new byte[1024];
                int length = 0;
                while ((length = inputStream.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, length);
                }
                outputStream.flush();
                outputStream.close();
                Log.d("DB Copy: ", "success");
                isSucess = true;

            } catch (IOException e) {
                e.printStackTrace();
                Log.d("DB Copy: ", "failed");
                isSucess = false;
            }
        } else {
            Log.d("DB Copy: ", "existing");
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mPDialog = ProgressDialog.show(myContext, "Working", "Creating your database");
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        mPDialog.dismiss();
        if (isSucess) {
            Toast.makeText(myContext, "Database successfully created", Toast.LENGTH_SHORT).show();
        }
        Log.d("CopyDataBase: ", "done");
    }

}
