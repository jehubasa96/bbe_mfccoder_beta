package com.jayzero.mfccoder.bbebymfccoder;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class NavigationDrawerFragment extends Fragment implements NavigationDrawerListAdapter.ClickListner {
    private static final String PREF_FILE_NAME = "tespref";
    private static final String KEY_USER_LEARNED_DRAWER = "userlearned";
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View containerView;
    private RecyclerView mDrawerList;
    private NavigationDrawerListAdapter mNavigationDrawerListAdapter;

    private boolean mUserLearnedDrawer;
    private boolean mFromSaveInstanceState;

    public NavigationDrawerFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLearnedDrawer = Boolean.valueOf(readFromPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, "false"));

        if (savedInstanceState != null) {
            mFromSaveInstanceState = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        mDrawerList = (RecyclerView) v.findViewById(R.id.navDrawerList);
        mNavigationDrawerListAdapter = new NavigationDrawerListAdapter(getActivity(), getData());
        mNavigationDrawerListAdapter.setClickListener(this);
        mDrawerList.setAdapter(mNavigationDrawerListAdapter);
        mDrawerList.setLayoutManager(new LinearLayoutManager(getActivity()));
        return v;
    }

    @Override
    public void navItemClicked(View v, int position) {
        switch (position) {
            case 0:
                Toast.makeText(getActivity(), "Old Testament", Toast.LENGTH_SHORT).show();
                Intent n = new Intent(getContext(), OldTestamentActivity.class);
                startActivity(n);
                break;
            case 1:
                Toast.makeText(getActivity(), "New Testament", Toast.LENGTH_SHORT).show();
                Intent o = new Intent(getContext(), NewTestamentActivity.class);
                startActivity(o);
                break;
            case 2:
                Toast.makeText(getActivity(), "Search", Toast.LENGTH_SHORT).show();
                Intent s = new Intent(getContext(), SearchActivity.class);
                startActivity(s);
                break;
        }
    }

    public static List<NavigationDrawerListName> getData() {
        List<NavigationDrawerListName> navList = new ArrayList<>();
        String[] titles = {"Old Testament", "New Testament", "Search"};
        int[] icon = {R.drawable.iconmonstr_new, R.drawable.iconmonstr_old, R.drawable.iconmonstr_magnifier_1_240};
        for (int x = 0; x < titles.length && x < icon.length; x++) {
            NavigationDrawerListName n = new NavigationDrawerListName();
            n.icon = icon[x];
            n.title = titles[x];
            navList.add(n);
        }
        return navList;
    }


    public void setup(int fragmentID, DrawerLayout drawer, Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentID);
        mDrawerLayout = drawer;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!mUserLearnedDrawer) {
                    mUserLearnedDrawer = true;
                    saveToPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, mUserLearnedDrawer + "");
                }

                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }
        };

        if (!mUserLearnedDrawer && !mFromSaveInstanceState) {
            mDrawerLayout.openDrawer(containerView);
        }
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    public static void saveToPreferences(Context context, String preferenceName, String preferenceValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(preferenceName, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(preferenceName, context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }
}
