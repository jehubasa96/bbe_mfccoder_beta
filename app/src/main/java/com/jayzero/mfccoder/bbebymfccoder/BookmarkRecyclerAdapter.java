package com.jayzero.mfccoder.bbebymfccoder;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by jayzero on 29/07/2016.
 */

public class BookmarkRecyclerAdapter extends RecyclerView.Adapter<BookmarkRecyclerAdapter.BookmarkVH> {
    private LayoutInflater layoutInflater;
    private List<BookmarkContents> bookmarklist = Collections.emptyList();
    private Context context;

    public BookmarkRecyclerAdapter(Context context, List<BookmarkContents> l) {
        layoutInflater = LayoutInflater.from(context);
        bookmarklist = l;
        this.context = context;
    }

    @Override
    public BookmarkVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.bookmark_vh, parent, false);
        BookmarkVH b = new BookmarkVH(v);
        return b;
    }

    @Override
    public void onBindViewHolder(final BookmarkVH holder, int position) {
        BookmarkContents b = bookmarklist.get(position);
        Log.d("onBind: ", String.valueOf(position));
        holder.book.setText(b.book);
        holder.chapter.setText(String.valueOf(b.chapter));
        holder.verse.setText(String.valueOf(b.verse));
        holder.text.setText(b.text);

        holder.v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                CharSequence[] options = {"Remove as bookmark", "Send as SMS", "Copy to Clipboard"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("OPTION").setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                BookMarkDatabaseHelper bDatabase = new BookMarkDatabaseHelper(context);
                                BookmarkContents bcontents = bookmarklist.get(holder.getAdapterPosition());
                                int result = bDatabase.deleteData(String.valueOf(bcontents.id));
                                if (result > 0) {
                                    Toast.makeText(context, "Item removed", Toast.LENGTH_SHORT).show();
                                    bookmarklist.remove(holder.getAdapterPosition());
                                    notifyItemRemoved(holder.getAdapterPosition());
                                    notifyItemRangeChanged(holder.getAdapterPosition(), bookmarklist.size());
                                } else {
                                    Toast.makeText(context, "Removing item failed", Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 1:
                                BookmarkContents bSendContents = bookmarklist.get(holder.getAdapterPosition());
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse("smsto:"));
                                intent.putExtra("sms_body", bSendContents.book + " " + bSendContents.chapter + ":" + bSendContents.verse + "\n" + bSendContents.text);
                                context.startActivity(intent);
                                break;
                            case 2:
                                BookmarkContents bCopyContents = bookmarklist.get(holder.getAdapterPosition());
                                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                                ClipData clipData = ClipData.newPlainText("copy", bCopyContents.book + " " + bCopyContents.chapter + ":" + bCopyContents.verse + "\n" + bCopyContents.text);
                                clipboard.setPrimaryClip(clipData);

                                if (clipboard.hasPrimaryClip()) {
                                    Toast.makeText(context, "Copied to Clipboard", Toast.LENGTH_SHORT).show();
                                }
                                break;
                        }
                    }
                });

                AlertDialog a = builder.create();
                a.show();

                return false;
            }
        });
    }


    @Override
    public int getItemCount() {
        return bookmarklist.size();
    }

    class BookmarkVH extends RecyclerView.ViewHolder {
        TextView book, chapter, verse, text;
        View v;

        public BookmarkVH(View itemView) {
            super(itemView);
            v = itemView;
            book = (TextView) itemView.findViewById(R.id.bookText);
            chapter = (TextView) itemView.findViewById(R.id.chapterText);
            verse = (TextView) itemView.findViewById(R.id.verseText);
            text = (TextView) itemView.findViewById(R.id.bibleText);
        }
    }

}
