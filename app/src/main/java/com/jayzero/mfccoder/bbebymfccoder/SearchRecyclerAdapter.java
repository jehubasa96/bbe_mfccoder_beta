package com.jayzero.mfccoder.bbebymfccoder;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by jayzero on 28/07/2016.
 */

public class SearchRecyclerAdapter extends RecyclerView.Adapter<SearchRecyclerAdapter.SearchVH> {
    private LayoutInflater layoutInflater;
    private Context context;
    private List<SearchResultContents> mScontents = Collections.emptyList();

    String[] books = {"GENESIS", "EXODUS", "LEVITICUS", "NUMBERS", "DEUTERONOMY", "JOSHUA", "JUDGES"
            , "RUTH", "1 SAMUEL", "2 SAMUEL", "1 KINGS", "2 KINGS", "1 CHRONICLES", "2 CHRONICLES", "EZRA"
            , "NEHEMIAH", "ESTHER", "JOB", "PSALMS", "PROVERBS", "ECCLESIASTES", "SONG OF SONGS", "ISAIAH"
            , "JEREMIAH", "LAMENTATIONS", "EZEKIEL", "DANIEL", "HOSEA", "JOEL", "AMOS", "OBADIAH", "JONAH"
            , "MICAH", "NAHUM", "HABAKKUK", "ZEPHANIAH", "HAGGAI", "ZECHARIAH", "MALACHI", "MATTHEW", "MARK"
            , "LUKE", "JOHN", "ACTS", "ROMANS", "1 CORINTHIANS", "2 CORINTHIANS", "GALATIANS", "EPHESIANS"
            , "PHILIPPIANS", "COLOSSIANS", "1 THESSALONIANS", "2 THESSALONIANS", "1 TIMOTHY", "2 TIMOTHY"
            , "TITUS", "PHILEMON", "HEBREWS", "JAMES", "1 PETER", "2 PETER", "1 JOHN", "2 JOHN", "3 JOHN"
            , "JUDE", "REVELATION"};

    public SearchRecyclerAdapter(Context context, List<SearchResultContents> list) {
        layoutInflater = LayoutInflater.from(context);
        mScontents = list;
        this.context = context;
    }

    @Override
    public SearchVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.search_vh, parent, false);
        SearchVH s = new SearchVH(v);
        return s;
    }

    @Override
    public void onBindViewHolder(final SearchVH holder, int position) {
        SearchResultContents s = mScontents.get(position);
        Log.d("onBind", String.valueOf(position));
        holder.book.setText(s.book);
        holder.chapter.setText(String.valueOf(s.chapter));
        holder.verse.setText(String.valueOf(s.verse));
        holder.text.setText(s.text);

        holder.v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                CharSequence[] options = {"Bookmark", "Send as SMS", "Copy to Clipboard"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("OPTION").setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                BookMarkDatabaseHelper bookmark = new BookMarkDatabaseHelper(context);
                                SearchResultContents bookm = mScontents.get(holder.getAdapterPosition());
                                boolean result = bookmark.insertData(bookm.id, Arrays.asList(books).indexOf(bookm.book), bookm.chapter, bookm.verse, bookm.text);
                                if (result) {
                                    Toast.makeText(context, "Saved as bookmark", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(context, "Saving as bookmark failed", Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 1:
                                SearchResultContents sSendContents = mScontents.get(holder.getAdapterPosition());
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse("smsto:"));
                                intent.putExtra("sms_body", sSendContents.book + " " + sSendContents.chapter + ":" + sSendContents.verse + "\n" + sSendContents.text);
                                context.startActivity(intent);
                                break;
                            case 2:
                                SearchResultContents sCopyContents = mScontents.get(holder.getAdapterPosition());
                                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                                ClipData clipData = ClipData.newPlainText("copy", sCopyContents.book + " " + sCopyContents.chapter + ":" + sCopyContents.verse + "\n" + sCopyContents.text);
                                clipboard.setPrimaryClip(clipData);

                                if (clipboard.hasPrimaryClip()) {
                                    Toast.makeText(context, "Copied to Clipboard", Toast.LENGTH_SHORT).show();
                                }
                                break;
                        }
                    }
                });

                AlertDialog a = builder.create();
                a.show();

                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mScontents.size();
    }

    public class SearchVH extends RecyclerView.ViewHolder {
        TextView book, chapter, verse, text;
        View v;

        public SearchVH(View itemView) {
            super(itemView);
            v = itemView;
            book = (TextView) itemView.findViewById(R.id.bookText);
            chapter = (TextView) itemView.findViewById(R.id.chapterText);
            verse = (TextView) itemView.findViewById(R.id.verseText);
            text = (TextView) itemView.findViewById(R.id.bibleText);
        }
    }
}
