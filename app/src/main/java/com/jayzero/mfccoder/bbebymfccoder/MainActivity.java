package com.jayzero.mfccoder.bbebymfccoder;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private RecyclerView bookmarkRecycler;
    private BookmarkRecyclerAdapter bookmarkRecyclerAdapter;
    private BookMarkDatabaseHelper mBMHelper;
    private TextView randBook, randChapter, randVerse, randText;
    private ScrollView mScrollView;

    OTDatabaseHelper mDBHelper;

    boolean isDoubleClick = false;

    Toolbar mToolbar;
    DrawerLayout mDrawerLayout;

    String[] books = {"GENESIS", "EXODUS", "LEVITICUS", "NUMBERS", "DEUTERONOMY", "JOSHUA", "JUDGES"
            , "RUTH", "1 SAMUEL", "2 SAMUEL", "1 KINGS", "2 KINGS", "1 CHRONICLES", "2 CHRONICLES", "EZRA"
            , "NEHEMIAH", "ESTHER", "JOB", "PSALMS", "PROVERBS", "ECCLESIASTES", "SONG OF SONGS", "ISAIAH"
            , "JEREMIAH", "LAMENTATIONS", "EZEKIEL", "DANIEL", "HOSEA", "JOEL", "AMOS", "OBADIAH", "JONAH"
            , "MICAH", "NAHUM", "HABAKKUK", "ZEPHANIAH", "HAGGAI", "ZECHARIAH", "MALACHI", "MATTHEW", "MARK"
            , "LUKE", "JOHN", "ACTS", "ROMANS", "1 CORINTHIANS", "2 CORINTHIANS", "GALATIANS", "EPHESIANS"
            , "PHILIPPIANS", "COLOSSIANS", "1 THESSALONIANS", "2 THESSALONIANS", "1 TIMOTHY", "2 TIMOTHY"
            , "TITUS", "PHILEMON", "HEBREWS", "JAMES", "1 PETER", "2 PETER", "1 JOHN", "2 JOHN", "3 JOHN"
            , "JUDE", "REVELATION"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("copy", "copy start");
        CopyDataBase copyDataBase = new CopyDataBase(this);
        copyDataBase.execute();
        Log.d("copy", "copy finish");


        mDBHelper = new OTDatabaseHelper(this);
        if (mDBHelper.checkDataBase()) {
            onGenerateRandomVerse();
        }

        mScrollView = (ScrollView) findViewById(R.id.mainScrollView);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.navDrawerLayout);
        mToolbar = (Toolbar) findViewById(R.id.customToolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationDrawerFragment mNavigation = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navDrawerFragment);
        mNavigation.setup(R.id.navDrawerFragment, mDrawerLayout, mToolbar);

        mBMHelper = new BookMarkDatabaseHelper(this);

        bookmarkRecycler = (RecyclerView) findViewById(R.id.bookMarkRecycler);
        Log.d("main: ", "1");
        bookmarkRecyclerAdapter = new BookmarkRecyclerAdapter(this, mBMHelper.getBookmarks());
        Log.d("main: ", "2");
        bookmarkRecycler.setAdapter(bookmarkRecyclerAdapter);
        bookmarkRecycler.setLayoutManager(new LinearLayoutManager(this));
        mScrollView.smoothScrollTo(0, 0);
    }

    @Override
    public void onBackPressed() {
        if (isDoubleClick) {
            super.onBackPressed();
            finish();
        }
        isDoubleClick = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isDoubleClick = false;
            }
        }, 2000);
    }

    public void onGenerateRandomVerse() {
        randBook = (TextView) findViewById(R.id.randBook);
        randChapter = (TextView) findViewById(R.id.randChapter);
        randVerse = (TextView) findViewById(R.id.randVerse);
        randText = (TextView) findViewById(R.id.randText);

        int book = (int) (Math.random() * 31102 + 0);
        mDBHelper.openDatabase();
        mDBHelper.cursor = mDBHelper.mDatabase.rawQuery("SELECT * FROM " + mDBHelper.TABLE_NAME, null);
        mDBHelper.cursor.moveToPosition(book);
        Log.d("random: ", books[mDBHelper.cursor.getInt(1)]);
        randBook.setText(books[mDBHelper.cursor.getInt(1)]);
        randChapter.setText(String.valueOf(mDBHelper.cursor.getInt(2)));
        randVerse.setText(String.valueOf(mDBHelper.cursor.getInt(3)));
        randText.setText(mDBHelper.cursor.getString(4));
    }
}
